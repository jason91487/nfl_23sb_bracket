! /bin/python

divisionals = [['chiefs', 'jags'], ['bengals', 'bills'], ['eagles', 'giants'],
               ['cowboys', 'niners']]
conference = []
bets = {
    'jason': [{
        'niners': 14,
        'bills': 13,
        'chiefs': 12,
        'eagles': 11,
        'cowboys': 10,
        'bengals': 9,
        'giants': 5,
        'jags': 3
    }, 108],
    'gizzard': [{
        'niners': 14,
        'bills': 11,
        'chiefs': 10,
        'eagles': 12,
        'cowboys': 7,
        'bengals': 13,
        'giants': 9,
        'jags': 5
    }, 118],
    'seb': [{
        'niners': 14,
        'bills': 12,
        'chiefs': 13,
        'eagles': 11,
        'cowboys': 10,
        'bengals': 7,
        'giants': 5,
        'jags': 6
    }, 108],
    'alex': [{
        'niners': 14,
        'bills': 13,
        'chiefs': 11,
        'eagles': 12,
        'cowboys': 8,
        'bengals': 7,
        'giants': 3,
        'jags': 9
    }, 116]
}

players = bets.keys()
def calculate_max(games, bets, multiplier, filter=None):
    scores = []
    for i in range(2**len(games)):
        scores.append(['', 0])
    max = ['', 0]
    divisonal_multiplies = 4
    i = 0
    for score in scores:
        j = 0
        for game in games:
            team = game[((i & (0x1 << j)) >> j)]
            score[1] += bets[team]
            score[0] += team + ' '
            j += 1
        i += 1
        if score[1] >= max[1]:
            max = score
    max[1] *= multiplier
    return (scores, max)

def max_score(bets):
    total = 0
    scores, max = calculate_max(divisionals, bets[0], 4)
    total += max[1]
    print(max)
    
    teams = max[0].split(' ')
    conference = [[teams[0], teams[1]], [teams[2], teams[3]]]
    scores, max = calculate_max(conference, bets[0], 6)
    total += max[1]
    print(max)
    
    teams = max[0].split(' ')
    bowl = [[teams[0], teams[1]]]
    scores, max = calculate_max(bowl, bets[0], 10)
    total += max[1]
    print(max, bets[1] + total)


if __name__ == '__main__':
    for player in players:
        print('-------------', player, '----------------')
        max_score(bets[player])